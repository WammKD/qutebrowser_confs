#!/usr/bin/env dash

# title="$(wget -qO- "$1" | perl -l -0777 -ne 'print $1 if /<title.*?>\s*(.*?)\s*<\/title/si')"

cd ~/Downloads/.p && yt-dlp -f "bestvideo[ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]/best" -o "%(title)s (%(id)s) – %(uploader)s.%(ext)s" "$1" 
